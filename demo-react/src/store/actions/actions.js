import { TOGGLE_ERROR_STATE, UPDATE_NAME, UPDATE_PASSWORD, ADD_CHECKBOX, REMOVE_CHECKBOX, UPDATE_DATE } from './actionTypes';

let errorState = false;

export const toggleErrorState = () => {
    errorState = !errorState;
    return {
        type: TOGGLE_ERROR_STATE,
        payload: { errorState: errorState }
    }
}

export const updateName = name => {
    return {
        type: UPDATE_NAME,
        payload: { name: name }
    }
}

export const updatePassword = password => {
    return {
        type: UPDATE_PASSWORD,
        payload: { password: password }
    }
}

export const addCheckbox = value => {
    return {
        type: ADD_CHECKBOX,
        payload: { value: value }
    }
}

export const removeCheckbox = value => {
    return {
        type: REMOVE_CHECKBOX,
        payload: { value: value }
    }
}

export const updateDate = date => {
    return {
        type: UPDATE_DATE,
        payload: { date: date }
    }
}
