import { combineReducers } from 'redux';
import errorState from './errorState';
import fields from './fields';

export default combineReducers({
    errorState,
    fields
})
