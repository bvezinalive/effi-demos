import { TOGGLE_ERROR_STATE } from '../actions/actionTypes';

const initialState = {
    errorState: false,
};

export default (state = initialState, action) => {
    switch (action.type) {
        case TOGGLE_ERROR_STATE:
            return action.payload.errorState;
        default:
            return state;
    }
}
