import { UPDATE_NAME, UPDATE_PASSWORD, ADD_CHECKBOX, REMOVE_CHECKBOX, UPDATE_DATE } from '../actions/actionTypes';

const initialState = {
    name: '',
    password: '',
    checkboxes: [],
    date: new Date(),
};

export default (state = initialState, action) => {
    switch(action.type) {
        case UPDATE_NAME:
            return {
                ...state,
                name: action.payload.name
            };
        case UPDATE_PASSWORD:
            return {
                ...state,
                password: action.payload.password
            };
        case ADD_CHECKBOX:
            return {
                ...state,
                checkboxes: [...state.checkboxes, action.payload.value]
            };
        case REMOVE_CHECKBOX:
            let newCheckboxes = [];
            for (let checkbox of state.checkboxes) {
                if (checkbox !== action.payload.value) {
                    newCheckboxes = [...newCheckboxes, checkbox];
                }
            }
            return {
                ...state,
                checkboxes: newCheckboxes
            };
        case UPDATE_DATE:
            return {
                ...state,
                date: action.payload.date
            }
        default:
            return state;
    }
}
