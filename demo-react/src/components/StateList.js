import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Grid, List, ListItem, ListItemText } from '@material-ui/core';
import '../App.css';

export default class StateList extends React.Component {
    constructor(props) {
        super(props);

        this.classes = makeStyles(theme => ({
            root: {
              flexGrow: 1,
              maxWidth: 752,
            },
            demo: {
              backgroundColor: theme.palette.background.paper,
            },
            title: {
              margin: theme.spacing(4, 0, 2),
            },
          }));
    }

    render() {
        return (
            <div id='state-list-component'>
                <Grid container spacing={2}>
                    <Grid item xs={12} md={6}>
                        <Typography variant="h6" className={this.classes.title}>
                            List of state fields
                        </Typography>
                        <div className={this.classes.demo}>
                            <List>
                                <ListItem>
                                    <ListItemText primary={"Error State: " + this.props.errorState.toString()} />
                                </ListItem>
                                <ListItem>
                                    <ListItemText primary={"Name: " + this.props.name} />
                                </ListItem>
                                <ListItem>
                                    <ListItemText primary={"Password: " + this.props.password} />
                                </ListItem>
                                <ListItem>
                                    <ListItemText primary={'Checkboxes: ' + this.props.checkboxes.toString() } />
                                </ListItem>
                                <ListItem>
                                    <ListItemText primary={'Date: ' + this.props.date}/>
                                </ListItem>
                            </List>
                        </div>
                    </Grid>
                </Grid>
            </div>
        )
    }
}
