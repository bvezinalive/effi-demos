import React from 'react';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import { TextField, Checkbox, FormGroup, FormControlLabel, Grid } from '@material-ui/core';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import { green } from '@material-ui/core/colors';
import '../App.css';

const classes = makeStyles(theme => ({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    textField: {
        width: 200,
    },
  }));

const GreenCheckbox = withStyles({
        root: {
            color: green[400],
            '&$checked': {
            color: green[600],
            },
        },
        checked: {},
    })(props => <Checkbox color="default" {...props} />);

export default class Form extends React.Component {
    constructor(props) {
        super(props);

        this.handleNameChange = this.handleNameChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
        this.handleDatePicker = this.handleDatePicker.bind(this);
    }

    toggleErrorState = this.props.toggleErrorState;

    handleNameChange(e) {
        this.props.updateName(e.target.value);
    }

    handlePasswordChange(e) {
        this.props.updatePassword(e.target.value);
    }

    handleCheckboxChange(e) {
        if (e.target.checked) {
            this.props.addCheckbox(e.target.value);
        } else {
            this.props.removeCheckbox(e.target.value);
        }
    }

    handleDatePicker(date) {
        this.props.updateDate(date);
    }

    render() {
        const element = (
            <div id="form-component">
                <form className={classes.root} noValidate autoComplete="off">
                    <div>
                        <TextField 
                            className={classes.textField}
                            error={this.props.errorState}
                            value={this.props.name}
                            onChange={this.handleNameChange}
                            label="Entrez votre nom"
                            variant="outlined"
                            margin="dense" />
                    </div>
                    <div>
                        <TextField
                            className={classes.textField}
                            error={this.props.errorState}
                            value={this.props.password}
                            onChange={this.handlePasswordChange}
                            label="Entrez votre mot de passe"
                            type="password"
                            variant="outlined"
                            margin="dense" />
                    </div>
                    <div>
                        <FormGroup row>
                            <Checkbox value="no label" onChange={this.handleCheckboxChange} />
                            <FormControlLabel
                                control={ <Checkbox value="primary" color="primary" /> }
                                onChange={this.handleCheckboxChange}
                                label="Primary" />
                            <FormControlLabel
                                control={ <Checkbox value="secondary" color="secondary" /> }
                                onChange={this.handleCheckboxChange}
                                label="Secondary" />
                            <FormControlLabel
                                control={ <GreenCheckbox value="custom color" /> }
                                onChange={this.handleCheckboxChange}
                                label="Custom color" />
                        </FormGroup>
                    </div>
                    <div>
                        <MuiPickersUtilsProvider utils={DateFnsUtils}>
                            <Grid container justify="space-around">
                                <KeyboardDatePicker
                                    onChange={this.handleDatePicker}
                                    value={this.props.date}
                                    margin="normal"
                                    id="date-picker-dialog"
                                    label="Date picker dialog"
                                    format="MM/dd/yyyy" />
                            </Grid>
                        </MuiPickersUtilsProvider>
                    </div>
                </form>
            </div>
        );
        return element;
    }
}
