import React from 'react';
import '../App.css';

export default class Header extends React.Component {

    render() {
        const element = (
            <div id="header-component">
                <h1>Form demo with React </h1>
                <h3>State Management with Redux | Premade components from Material UI</h3>
            </div>
        );
        return element
    }
}
