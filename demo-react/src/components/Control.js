import React from 'react';
import { Button } from '@material-ui/core';
import '../App.css';

export default class Control extends React.Component {
    constructor(props) {
        super(props);
        this.toggleErrorState = this.toggleErrorState.bind(this);
    }

    toggleErrorState() {
        this.props.toggleErrorState();
    }

    render() {
        return (
            <div id='control-component'>
                <Button variant="contained" onClick={this.toggleErrorState}>Toggle Error State</Button>
            </div>
        )
    }
}
