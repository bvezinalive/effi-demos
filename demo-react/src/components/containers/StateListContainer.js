import { connect } from 'react-redux';
import StateList from '../StateList';

const mapState = state => ({
    errorState: state.errorState,
    name: state.fields.name,
    password: state.fields.password,
    checkboxes: state.fields.checkboxes,
    date: state.fields.date
})

export default connect(mapState)(StateList);
