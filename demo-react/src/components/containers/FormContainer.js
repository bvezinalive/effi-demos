import { connect } from 'react-redux';
import Form from '../Form';
import { updateName, updatePassword, addCheckbox, removeCheckbox, updateDate } from '../../store/actions/actions';

const mapState = state => ({
    errorState: state.errorState,
    date: state.fields.date
})

const mapDispatch = {
    updateName,
    updatePassword,
    addCheckbox,
    removeCheckbox,
    updateDate
};

export default connect(mapState, mapDispatch)(Form)
