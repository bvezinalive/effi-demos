import { connect } from 'react-redux';
import Control from '../Control';
import { toggleErrorState } from '../../store/actions/actions';

const mapDispatch = {
    toggleErrorState,
}

export default connect(null, mapDispatch)(Control);
