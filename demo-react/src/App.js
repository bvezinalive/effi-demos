import React from 'react';
import './App.css';
import Header from './components/Header';
import Form from './components/containers/FormContainer';
import StateList from './components/containers/StateListContainer';
import Control from './components/containers/ControlContainer';
import { Grid } from '@material-ui/core';

export default class App extends React.Component {

	render() {
		return (
			<div className="App">
				<Header />
				<Grid container spacing={2}>
					<Grid item xs={4}>
						<StateList />
					</Grid>
					<Grid item xs={4}>
						<Form />
					</Grid>
					<Grid item xs={4}>
						<Control />
					</Grid>
				</Grid>
			</div>
		)
	}
}
