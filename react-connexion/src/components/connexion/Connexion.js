import React, { useState } from 'react';
import { Grid, makeStyles, TextField, Checkbox, FormControlLabel, Button } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    formBody: {
        backgroundColor: '#ffffff',
        padding: '10px 0',
    },
    formElement: {
        margin: '10px 0',
    },
}))

export default function Connexion() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [isRemembered, setIsRemembered] = useState(false);
    const classes = useStyles();

    const handleEmail = (event) => {
        setEmail(event.target.value);
    }

    const handlePassword = (event) => {
        setPassword(event.target.value);
    }

    const handleIsRemembered = (event) => {
        const { checked } = event.target
        setIsRemembered(checked);
    }

    const handleConnexion = (event) => {
        console.log('Email: ' + email);
        console.log('Password: ' + password);
        console.log('Is Remembered: ' + isRemembered);
    }

    return (
        <div>
            <Grid container>
                <Grid item xs={false} sm={2} lg={4}></Grid>
                <Grid container item justify="center" >
                    <Grid item>
                        <img className={classes.formElement} src="./effi.png" alt="Effi Logo" />
                    </Grid>
                </Grid>
            </Grid>
            <Grid container>
                <Grid item xs={false} sm={2} lg={4}></Grid>
                <Grid className={classes.formBody} container item xs={12} sm={8} lg={4}>
                    <Grid container item justify="center">
                        <Grid item>
                        <h3>Connectez-vous en utilisant votre identifiant Effi</h3>
                        </Grid>
                    </Grid>
                    <Grid container item justify="center">
                        <Grid item>
                            <TextField
                                id="inputUsername"
                                className={classes.formElement}
                                variant="outlined"
                                label="Courriel"
                                placeholder="Entrez votre courriel"
                                onChange={handleEmail} />
                        </Grid>
                    </Grid>
                    <Grid container item justify="center">
                        <Grid item>
                            <TextField
                                id="inputPassword"
                                className={classes.formElement}
                                variant="outlined"
                                type="password"
                                label="Mot de passe"
                                placeholder="Entrez votre mot de passe"
                                helperText="Mot de passe oublié?"
                                onChange={handlePassword} />
                        </Grid>
                    </Grid>
                    <Grid container item justify="center" spacing={10}>
                        <Grid item>
                            <FormControlLabel 
                                control={<Checkbox className={classes.formElement} onChange={handleIsRemembered} />}
                                label="Se souvenir de moi" />
                        </Grid>
                        <Grid item>
                            <Button
                                className={classes.formElement}
                                variant="contained"
                                color="secondary" 
                                onClick={handleConnexion} >Connexion</Button>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        </div>
    )
}
