import React from 'react';
import { makeStyles } from '@material-ui/core';
import Connexion from './components/connexion/Connexion';
import { useWindowSize } from './CustomHooks';

const useStyles = makeStyles({
  rootLg: {
    position: 'relative',
    top: '25vh'
  },
})

const widthXs = 576;
const heightXs = 550;

function App() {
  const [windowWidth, windowHeight] = useWindowSize();
  const classes = useStyles();  

  return (
    <div className={windowWidth < widthXs || windowHeight < heightXs ? '' : classes.rootLg}>
      <Connexion/>
    </div>
  );
}

export default App;
