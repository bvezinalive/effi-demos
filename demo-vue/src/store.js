import Vuex from 'vuex'
import Vue from 'vue'

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        errorState: false,
        name: '',
        password: '',
        checkboxes: [],
        date: new Date().toISOString().substr(0, 10)
    },
    mutations: {
        updateErrorState(state) {
            state.errorState = !state.errorState
        },
        updateName(state, name) {
            state.name = name
        },
        updatePassword(state, password) {
            state.password = password
        },
        addCheckbox(state, checkbox) {
            state.checkboxes = [...state.checkboxes, checkbox]
        },
        removeCheckbox(state, checkbox) {
            let checkboxes = []
            for(let aCheckbox of state.checkboxes) {
                if (aCheckbox !== checkbox) {
                    checkboxes = [...checkboxes, aCheckbox]
                }
            }
            state.checkboxes = checkboxes
        },
        updateDate(state, date) {
            state.date = date
        }
    }
})

export default store
