package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	_ "github.com/go-sql-driver/mysql"
)

type campus struct {
	name string
	address string
}

func main() {
	// Handler dont la Response sera toujours le directory "public"
	fs := http.FileServer(http.Dir("./public"))
	// On enregistre ce handler pour l'url "/public/"
		// On enlève le /public/ de l'URL pour passer seulement le fichier désiré au FileServer
	http.Handle("/public/", http.StripPrefix("/public/", fs))
	// Pour handler le form d'ajout de supplier
	http.HandleFunc("/add-campus", handleAddCampusForm)
	// Handle le ajax call pour afficher les campus
	http.HandleFunc("/ajaxGetCampuses", ajaxGetCampuses)
	// On lance le serveur en précisant quel port il écoute
	fmt.Println("Listening to port 8080...")
	err := http.ListenAndServe(":8080", nil)
	checkErr(err)
}

// Parse le post data du add-campus form pour insérer à la bd
func handleAddCampusForm(w http.ResponseWriter, r *http.Request) {
	// On prend le raw data de l'url pour populer r.Form
	err := r.ParseForm()
	checkErr(err)
	// On peut maintenant créer notre struct Campus en accédant aux champs
	campus := campus{r.Form["name"][0], r.Form["address"][0]}
	// Insérer à la BD ici
	fmt.Println("Campus à insérer:")
	fmt.Println(campus)
	// Open the connection to the database
	db, err := sql.Open("mysql", "root@tcp(localhost:3306)/demo_go_web_app_bv")
	checkErr(err)
	// Make sure the connection is working
	if err := db.PingContext(context.Background()); err != nil {
		fmt.Println("Connection didnt work!")
	}
	// Statement de insert ici
	result, err := db.ExecContext(
		context.Background(),
		"INSERT INTO campus (name, address) VALUES (?, ?)",
		campus.name,
		campus.address,
	)
	checkErr(err)
	fmt.Printf("%v\n", result)
	// On redirige au home
	http.Redirect(w, r, "/public/", 308)
}

func ajaxGetCampuses(w http.ResponseWriter, r *http.Request) {
	// Start by opening the connection and making sure its working
	db, err := sql.Open("mysql", "root@tcp(localhost:3306)/demo_go_web_app_bv")
	checkErr(err)
	if err := db.PingContext(context.Background()); err != nil {
		fmt.Println("Connection didnt work!")
	}
	// Select every campuses
	rows, err := db.QueryContext(context.Background(), "SELECT name, address FROM campus")
	checkErr(err)
	// Close the rows when we exit the function
	defer rows.Close()
	// Go through every row to build our response
	var campuses []map[string]string
	for rows.Next() {
		var name, address string
		if err := rows.Scan(&name, &address); err != nil {
			log.Fatal(err)
		}
		campuses = append(campuses, map[string]string{"name": name, "address": address})
	}
	// Convert campuses to JSON
	jsonCampuses, err := json.Marshal(campuses)
	checkErr(err)
	// Set the content type for json
	w.Header().Set("Content-Type", "application/json")
	_, err = w.Write(jsonCampuses)
	checkErr(err)
}

// Just to save some lines of code
func checkErr(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
